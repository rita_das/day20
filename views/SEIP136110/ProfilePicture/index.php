<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136110\ProfilePicture\ImageUpload;
use App\Bitm\SEIP136110\Message\Message;
use App\Bitm\SEIP136110\Utility\Utility;

$obj = new ImageUpload();
$allInfo = $obj->index();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>User Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <center><h2>User Details</h2></center>
    <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])): ?>
    <div class="alert alert-info" id="message">
       <center> <?php echo Message::message() ?> </center>
    </div>
    <?php endif; ?>

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-info" role="button">View Trashlist</a>
   <!-- <p>Contextual classes can be used to color table rows or table cells. The classes that can be used are: .active, .success, .info, .warning, and .danger.</p>
-->    <table class="table">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Username</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $sl = 0;
        foreach ($allInfo as $info) { $sl++; ?>
        <tr class="success">
            <td><?php echo $sl ?></td>
            <td><?php echo $info->id  ?></td>
            <td><?php echo $info->name  ?></td>
            <td>
                <img src="../../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px" width="100px">
            </td>
            <td>
                <a href="view.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">View</a>
                <a href="edit.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Update</a>
                <a href="trash.php?id=<?php echo $info->id ?>" class="btn btn-warning" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
       <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $('#message').show().delay(1100).fadeOut();
</script>

</body>
</html>

