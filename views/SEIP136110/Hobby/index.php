<?php

session_start();

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP136110\Hobby\Hobby;
use App\Bitm\SEIP136110\Message\Message;
use App\Bitm\SEIP136110\Utility\Utility;


$obj =new Hobby();
$obj->prepare($_POST);
$allHobby = $obj->index();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <center><h2>Subscribers Hobbies</h2></center>
   <!-- <p>
        Contextual classes can be used to color table rows or table cells.
        The classes that can be used are: .active, .success, .info, .warning, and .danger.
    </p> -->

    <a href="create.php" class="btn btn-primary" role="button">Create Again</a>
    <a href="trashlist.php" class="btn btn-primary" role="button">View Trash List</a>

    <div class="alert alert-info" id="message">
        <center><?php
            if(array_key_exists("message",$_SESSION) && (!empty($_SESSION['message'])))
            echo Message::message()
            ?></center>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count =0 ;
        foreach ($allHobby as $item) { $count++;
            ?>
        <tr class="success">


            <td><?php echo $count?> </td>
            <td> <?php echo $item->id ?> </td>
            <td> <?php echo $item->hobby ?> </td>
            <td>
                <a href="view.php?id=<?php echo $item->id?>" class="btn btn-primary" role="button">View</a>
                <a href="edit.php?id=<?php echo $item->id?>" class="btn btn-info" role="button">Update</a>
                <a href="trash.php?id=<?php echo $item->id?>" class="btn btn-warning" role="button">Trash</a>
                <a href="delete.php?id=<?php echo $item->id?>" class="btn btn-danger" role="button" Onclick="return ConfirmDelete()">Delete</a>
            </td>
        </tr>
       <?php } ?>
        </tbody>
    </table>
</div>
    <script>
        $('#message').show().delay(2000).fadeOut();

        function ConfirmDelete()
        {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

    </script>

</body>
</html>